'use strict';

var assert = require("assert")
var persist = require("persist")

describe('prediction', function() {

	describe('#getDataFromDB', function() {

		it('should return data from db', function(done) {

			persist.connect(function(err, connection) {
				if (err) {
					next(err);
					return;
				}

				var sql = "select distinct pickup_area, count(*) as `total_pickup` from histories group by pickup_area order by `total_pickup` limit 1";

				connection.runSqlAll(sql, [], function(err, results) {
					if (err) {
						next(err);
						return;
					}

					var strJson = results[0];
					console.log(strJson);
				});

				done();
			});
		});
	});
});
