var redisManager = require('redis-client-manager');
var redisClient = redisManager.getClient();

exports.push = function(strJson) {
	message = JSON.stringify(strJson);
	redisClient.lpush('predictions', message, function(err) {
		if (err) {
			next(err);
			return;
		}
	});
};

exports.pop = function() {
	return redisClient.rpop('predictions', function(err, result) {
		if (err) {
			next(err);
			return;
		}
	});
}

exports.publish = function(strJson) {
	message = JSON.stringify(strJson);
	redisClient.publish('pubsub-channel', message, function(err) {
		if (err) {
			next(err);
			return;
		}
	});
}

exports.subscribe = function() {
	redisClient.subscribe('pubsub-channel');
	redisClient.on("message", function(channel, message) {
		console.log(message)
	});

}
